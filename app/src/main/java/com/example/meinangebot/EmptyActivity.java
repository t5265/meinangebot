package com.example.meinangebot;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Empty activity is used for navigation purpose.
 * @author Laurin Sasse
 */
public class EmptyActivity extends AppCompatActivity {

    @SuppressLint("StaticFieldLeak")
    public static Activity fa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        endActivity(extras.getInt("Activity_Int"));
        fa = this;
        Intent intent1;
        intent1 = openActivity(extras.getInt("Activity_Int"));
        intent1.putExtra("Activity_Int", 4);
        intent1.putParcelableArrayListExtra("MoreInformation",
                intent.getParcelableArrayListExtra("MoreInformation"));
        startActivity(intent1);
        overridePendingTransition(0,0);
    }
    /**
     * The previous activity will be closed.
     * The argument i identifies the activity, that will be closed.
     * @param i identifier to close the previous activity
     */
    public void endActivity(int i) {
        switch (i) {
            case 0:
                HomeActivity.fa.finish();
                break;
            case 1:
                FavoriteActivity.fa.finish();
                break;
            case 2:
                ShoppingListActivity.fa.finish();
                break;
            case 3:
                break;
            case 4:
                EmptyActivity.fa.finish();
                break;
            case 5:
                ShoppingListActivity.fa.finish();
                break;
            case 6:
                HomeActivity.fa.finish();
                break;
        }
    }
    /**
     * Will open the next activity.
     * The argument i identifies the activity, that will be opened next.
     * @param i identifier to open the next activity
     */
    public Intent openActivity(int i) {
        Intent intent;
        switch (i) {
            case 0:
                intent = new Intent(getApplicationContext(),
                        HomeActivity.class);
                break;
            case 1:
            case 5:
            case 6:
                intent = new Intent(getApplicationContext(),
                        FavoriteActivity.class);
                break;
            case 2:
                intent = new Intent(getApplicationContext(),
                        ShoppingListActivity.class);
                break;
            default:
                intent = null;
                break;
        }
        return intent;
    }
}