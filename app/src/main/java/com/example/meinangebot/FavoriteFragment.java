package com.example.meinangebot;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.firebase.auth.FirebaseAuth;
import java.util.ArrayList;

/**
 * The FavoriteFragment displays all the favorite offers in the FavoriteActivity.
 * @author Laurin Sasse
 */
public class FavoriteFragment extends Fragment
        implements RecyclerViewAdapterFavorites.SwipeLeftListener,
        RecyclerViewAdapterFavorites.SwipeRightListener {

    private RecyclerView recyclerView;
    private FavoriteListener favoriteListener;

    /**
     * The interface FavoriteListener handles the communication
     * between the FavoriteFragment and the FavoriteActivity.
     */
    public interface FavoriteListener {
        void gesture(boolean delete, int position);
    }

    /**
     * This method displays all the favorite offers.
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return returns the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container,
                false);
        recyclerView = view.findViewById(R.id.recyclerView);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            setRecyclerView(((FavoriteActivity)
            view.getContext()).loadList("favorite_list", "favorite"));
        }
        else {
            setRecyclerView(((FavoriteActivity)
            view.getContext()).loadList("favorite_list_firebase", "favorite"));
        }
        return view;
    }

    /**
     * This method sets the RecyclerView.
     * @param list ArrayList, that transfers all the offers to the RecyclerView
     */
    public void setRecyclerView(ArrayList<Angebot> list) {
        RecyclerViewAdapterFavorites recyclerViewAdapterFavorites =
                new RecyclerViewAdapterFavorites(list, this, this);
        recyclerView.setAdapter(recyclerViewAdapterFavorites);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    /**
     * This method calls the method void gesture() in the FavoriteActivity,
     * that deletes the offer at the position.
     * @param position holds the information about the position
     */
    @Override
    public void swipeLeft(int position) {
        favoriteListener.gesture(true,position);
    }

    /**
     * This method calls the method void gesture(...) in the FavoriteActivity,
     * that adds the offer at the position to the shopping list.
     * @param position holds the information about the position
     */
    @Override
    public void swipeRight(int position) {
        favoriteListener.gesture(false,position);
    }

    /**
     * This method attaches favoriteListener.
     * @param context context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FavoriteFragment.FavoriteListener) {
            favoriteListener = (FavoriteFragment.FavoriteListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FavoriteListener");
        }
    }

    /**
     * This method detaches the favoriteListener.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        favoriteListener = null;
    }

}