package com.example.meinangebot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

/**
 * The FilterFragment is used to set the Filter according to user preferences.
 * @author Laurin Sasse
 */
public class FilterFragment extends Fragment {

    private String category;
    private String supermarket;
    private boolean ablaufdatum;
    private FilterFragmentListener filterFragmentListener;

    /**
     * The interface FilterFragmentListener handles the communication
     * between the FilterFragment and the HomeActivity.
     * Interface invokes the
     * method void applyFilters(String category, String supermarkt, boolean ablaufdatum)
     * in the HomeActivity.
     */
    public interface FilterFragmentListener {
        void applyFilters(String category, String supermarket, boolean ablaufdatum);
    }

    /**
     * This method displays the FilterFragment.
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return returns the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        Spinner spinner1 = view.findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.
                createFromResource(getContext(), R.array.category, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
        spinner1.setOnItemSelectedListener(new Category());
        Spinner spinner2 = view.findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.
                createFromResource(getContext(), R.array.supermarket, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);
        spinner2.setOnItemSelectedListener(new Supermarket());
        FloatingActionButton floatingActionButton1 = view.findViewById(R.id.floatingActionButton1);
        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterFragmentListener.applyFilters(category, supermarket, ablaufdatum);
            }
        });
        FloatingActionButton floatingActionButton2 = view.findViewById(R.id.floatingActionButton2);
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        category = null;
        supermarket = null;
        ablaufdatum = false;
        return view;
    }

    /**
     * If the user chooses a category, the method onItemSelected will be invoked.
     */
    class Category implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            category = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    /**
     * If the user chooses a supermarket, the method void onItemSelected(...) will be invoked.
     */
    class Supermarket implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            supermarket = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    /**
     * This method attaches filterFragmentListener.
     * @param context context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FilterFragment.FilterFragmentListener) {
            filterFragmentListener = (FilterFragment.FilterFragmentListener) context;
        }
        else {
            throw new RuntimeException(context.toString()
                    + " must implement FilterFragmentListener");
        }
    }

    /**
     * This method detaches the filterFragmentListener.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        filterFragmentListener = null;
    }

}