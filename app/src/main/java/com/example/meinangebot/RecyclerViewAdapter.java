package com.example.meinangebot;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * This class defines the RecyclerViewAdapter for recycler view in the HomeActivity.
 * Sources:
 *          Source: https://square.github.io/picasso/
 *          Library: com.squareup.picasso:picasso:2.71828
 *          Version: 2.71828
 *          License:
 *                      Copyright 2013 Square, Inc.
 *
 *          Licensed under the Apache License, Version 2.0 (the "License");
 *          you may not use this file except in compliance with the License.
 *          You may obtain a copy of the License at
 *
 *              http://www.apache.org/licenses/LICENSE-2.0
 *
 *          Unless required by applicable law or agreed to in writing, software
 *          distributed under the License is distributed on an "AS IS" BASIS,
 *          WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *          See the License for the specific language governing permissions and
 *          limitations under the License.
 *
 * @author Laurin Sasse
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final ArrayList<Angebot> list;
    private boolean gerade;
    private final ImageButton1Listener imageButton1Listener;
    private final ImageButton2Listener imageButton2Listener;

    public interface ImageButton1Listener {
        void imageButton1(int position);
    }

    public interface ImageButton2Listener {
        void imageButton2(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView1, textView2, textView3, textView4, border1, border2, border3, border4;
        ImageButton imageButton1, imageButton2;
        ImageButton1Listener imageButton1Listener;
        ImageButton2Listener imageButton2Listener;

        public ViewHolder(View view, ImageButton1Listener imageButton1Listener,
                          ImageButton2Listener imageButton2Listener) {
            super(view);
            this.imageButton1Listener = imageButton1Listener;
            this.imageButton2Listener = imageButton2Listener;
            textView1 = view.findViewById(R.id.textView1);
            textView2 = view.findViewById(R.id.textView2);
            textView3 = view.findViewById(R.id.textView3);
            textView4 = view.findViewById(R.id.textView4);
            border1 = view.findViewById(R.id.border1);
            border2 = view.findViewById(R.id.border2);
            border3 = view.findViewById(R.id.border3);
            border4 = view.findViewById(R.id.border4);
            imageButton1 = view.findViewById(R.id.imageButton1);
            imageButton2 = view.findViewById(R.id.imageButton2);
            imageButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageButton1Listener.imageButton1(getAdapterPosition());
                }
            });
            imageButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageButton2Listener.imageButton2(getAdapterPosition());
                }
            });
        }

        public TextView getTextView1() {
            return textView1;
        }

        public TextView getTextView2() {
            return textView2;
        }

        public TextView getTextView3() {
            return textView3;
        }

        public TextView getTextView4() {
            return textView4;
        }

        public TextView getBorder3() {
            return border3;
        }

        public TextView getBorder4() {
            return border4;
        }

        public ImageButton getImageButton1() {
            return imageButton1;
        }

        public ImageButton getImageButton2() {
            return imageButton2;
        }

    }

    public RecyclerViewAdapter(ArrayList<Angebot> list, ImageButton1Listener imageButton1Listener,
                               ImageButton2Listener imageButton2Listener) {
        this.list = list;
        this.imageButton1Listener = imageButton1Listener;
        this.imageButton2Listener = imageButton2Listener;
        gerade = true;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.angebot_dummy, viewGroup, false);
        return new ViewHolder(view, imageButton1Listener, imageButton2Listener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        if (gerade) {
            viewHolder.getTextView1().setText(list.get(2 * position).getTitle());
            Picasso.get().load(list.get(2 * position).getImage_url())
                    .resize(425, 425).centerCrop().into(viewHolder.getImageButton1());
            if (!list.get(2 * position).getSupermarket().equals("Rewe")) {
                viewHolder.getTextView3().setVisibility(View.INVISIBLE);
            }
            else {
                viewHolder.getTextView3().setVisibility(View.VISIBLE);
                viewHolder.getTextView3().setText(list.get(2 * position).getPreis() + "€");
                viewHolder.getTextView3().setTextColor(Color.parseColor("#FF0000"));
            }
            viewHolder.getTextView2().setText(list.get(2 * position + 1).getTitle());
            Picasso.get().load(list.get(2 * position + 1).getImage_url())
                    .resize(425, 425).centerCrop().into(viewHolder.getImageButton2());
            if (!list.get(2 * position + 1).getSupermarket().equals("Rewe")) {
                viewHolder.getTextView4().setVisibility(View.INVISIBLE);
            }
            else {
                viewHolder.getTextView4().setVisibility(View.VISIBLE);
                viewHolder.getTextView4().setText(list.get(2 * position + 1).getPreis() + "€");
                viewHolder.getTextView4().setTextColor(Color.parseColor("#FF0000"));
            }
        }
        else {
            viewHolder.getTextView1().setText(list.get(2 * position).getTitle());
            Picasso.get().load(list.get(2 * position).getImage_url()).
                    resize(425, 425).centerCrop().into(viewHolder.getImageButton1());
            if (!list.get(2 * position).getSupermarket().equals("Rewe")) {
                viewHolder.getTextView3().setVisibility(View.INVISIBLE);
            }
            else {
                viewHolder.getTextView3().setVisibility(View.VISIBLE);
                viewHolder.getTextView3().setText(list.get(2 * position).getPreis() + "€");
                viewHolder.getTextView3().setTextColor(Color.parseColor("#FF0000"));
            }
            if (position != getItemCount()-1) {
                viewHolder.getTextView2().setText(list.get(2 * position + 1).getTitle());
                Picasso.get().load(list.get(2 * position + 1).getImage_url()).
                        resize(425, 425).centerCrop().into(viewHolder.getImageButton2());
                if (!list.get(2 * position + 1).getSupermarket().equals("Rewe")) {
                    viewHolder.getTextView4().setVisibility(View.INVISIBLE);
                }
                else {
                    viewHolder.getTextView4().setVisibility(View.VISIBLE);
                    viewHolder.getTextView4().setText(list.get(2 * position + 1).getPreis() + "€");
                    viewHolder.getTextView4().setTextColor(Color.parseColor("#FF0000"));
                }
            }
            else {
                viewHolder.getTextView2().setVisibility(View.INVISIBLE);
                viewHolder.getImageButton2().setVisibility(View.INVISIBLE);
                viewHolder.getBorder3().setVisibility(View.INVISIBLE);
                viewHolder.getBorder4().setVisibility(View.INVISIBLE);
                viewHolder.getTextView4().setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (list.size() % 2 == 0) {
            gerade = true;
        }
        else {
            gerade = false;
        }
        return (int) Math.ceil(list.size()/2.0);
    }

}


