package com.example.meinangebot;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

/**
 * The FavoriteActivity manages all the favorite offers.
 * @author Laurin Sasse
 */
public class FavoriteActivity extends AppCompatActivity
        implements FavoriteFragment.FavoriteListener,
        LogInFragment.LogInFragmentListener {

    @SuppressLint("StaticFieldLeak")
    public static Activity fa;
    private ChildEventListener childEventListener;
    private boolean loggedIn;

    /**
     * This method displays all the favorite offers.
     * There are two cases. In the first case it shows the local favorites and
     * in the second case it shows the favorites, that are stored in a realtime
     * database (FireBase).
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            endActivity(extras.getInt("Activity_Int"));
        }
        fa = this;
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            loggedIn = false;
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container1, new FavoriteFragment())
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container1,
                        new FavoriteFragment(), "FAVORITE_FRAGMENT")
                        .commit();
            saveList(null,"favorite_list_firebase","favorite");
            loggedIn = true;
            updateData();
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container2, new LogInFragment(loggedIn))
                .commit();
        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.favorites);
        bottomNavigationView.setOnItemSelectedListener(navListener);
    }

    //For navigation purpose
    private final BottomNavigationView.OnItemSelectedListener navListener =
            new BottomNavigationView.OnItemSelectedListener() {
                @SuppressLint("NonConstantResourceId")
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Intent intent = null;
                    boolean favoriteActivity = false;
                    switch (item.getItemId()) {
                        case R.id.home:
                            intent = new Intent(getApplicationContext(),
                                    HomeActivity.class);
                            intent.putExtra("Activity_Int", 1);
                            if (loggedIn) {
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference reference = database.getReference("users").
                                    child((Objects.requireNonNull
                                    (FirebaseAuth.getInstance().getCurrentUser())).
                                    getUid()).child("favorites");
                                reference.removeEventListener(childEventListener);
                            }
                            break;
                        case R.id.favorites:
                            favoriteActivity = true;
                            if (loggedIn) {
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference reference = database.getReference("users").
                                    child((Objects.requireNonNull
                                    (FirebaseAuth.getInstance().getCurrentUser())).
                                    getUid()).child("favorites");
                                reference.removeEventListener(childEventListener);
                            }
                            break;
                        case R.id.shopping_list:
                            intent = new Intent(getApplicationContext(),
                                    ShoppingListActivity.class);
                            intent.putExtra("Activity_Int", 1);
                            if (loggedIn) {
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference reference = database.getReference("users").
                                                    child((Objects.requireNonNull
                                                    (FirebaseAuth.getInstance().getCurrentUser())).
                                                    getUid()).child("favorites");
                                reference.removeEventListener(childEventListener);
                            }
                            break;
                    }
                    if (!favoriteActivity) {
                        startActivity(intent);
                    } else {
                        intent = new Intent(getApplicationContext(), EmptyActivity.class);
                        intent.putExtra("Activity_Int", 1);
                        startActivity(intent);
                    }
                    overridePendingTransition(0, 0);
                    return true;
                }
            };
    //For navigation purpose
    public void endActivity(int i) {
        switch (i) {
            case 0:
                HomeActivity.fa.finish();
                break;
            case 1:
                FavoriteActivity.fa.finish();
                break;
            case 2:
                ShoppingListActivity.fa.finish();
                break;
            case 3:
                SignInActivity.fa.finish();
                break;
            case 4:
                EmptyActivity.fa.finish();
                break;
        }
    }

    /**
     * This method loads an ArrayList, that is stored in the local storage
     * of the device.
     * @param list list, that you want to load
     * @param key key in order to access the list
     * @return the list, that is loaded
     */
    public ArrayList<Angebot> loadList(String list, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(list,
                Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<Angebot>>() {
        }.getType();
        ArrayList<Angebot> x = gson.fromJson(json, type);
        if (x == null) {
            x = new ArrayList<>();
        }
        return x;
    }

    /**
     * This method overwrites an ArrayList to a list with a key.
     * The list is stored in the local storage of the device.
     * @param x ArrayList
     * @param list list, that you want to overwrite
     * @param key key in order to access the list
     */
    public void saveList(ArrayList<Angebot> x, String list, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(list,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(x);
        editor.putString(key, json);
        editor.apply();
    }

    /**
     * In the first place this method checks, if the user is logged in.
     * If not it adds or deletes favorite offers in order to the argument delete.
     * It only adds the offer to the shopping list,
     * if is not existing in the shopping list.
     * @param delete indicates if an offer should be send to shopping list
     * or if it should be deleted in the favorite list.
     * @param position holds the information about the position
     */
    @Override
    public void gesture(boolean delete, int position) {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            ArrayList<Angebot> list = loadList("favorite_list", "favorite");
            if (delete) {
                list.remove(position);
            } else {
                if (!isInShoppingList(position)) {
                    Angebot angebot = list.get(position);
                    list = loadList("shopping_list", "shopping");
                    list.add(angebot);
                    saveList(list, "shopping_list", "shopping");
                    return;
                }
            }
            saveList(list, "favorite_list", "favorite");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container1, new FavoriteFragment())
                    .commit();
        }
        else {
            if (delete) {
                ArrayList<Angebot> list = loadList("favorite_list_firebase",
                        "favorite");
                Angebot angebot = list.get(position);
                list.remove(position);
                String[] key = new String[1];
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference reference = database.getReference("users");
                reference.child(FirebaseAuth.getInstance().getCurrentUser().
                getUid()).child("favorites").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot data : snapshot.getChildren()) {
                            Angebot angebot_firebase = data.getValue(Angebot.class);
                            assert angebot_firebase != null;
                            if (angebot_firebase.getTitle().equals(angebot.getTitle())) {
                                key[0] = data.getKey();
                            }
                        }
                        saveList(list, "favorite_list_firebase", "favorite");
                        reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .child("favorites").child(key[0])
                                .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), "Favorit wurde entfernt.",
                                    Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Favorit konnte nicht entfernt werden.",
                                    Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(getApplicationContext(), "Favorit konnte nicht entfernt werden.",
                        Toast.LENGTH_LONG).show();
                    }
                });
            }
            else {
                ArrayList<Angebot> list = loadList("favorite_list_firebase", "favorite");
                if (!isInShoppingList(position)) {
                    Angebot angebot = list.get(position);
                    list = loadList("shopping_list", "shopping");
                    list.add(angebot);
                    saveList(list, "shopping_list", "shopping");
                }
            }
        }
    }

    /**
     * This method checks if the shopping list already contains a certain offer.
     * @param position holds the information about the position
     * @return returns true if it is already in the shopping list and false if not
     */
    public boolean isInShoppingList(int position) {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            ArrayList<Angebot> shoppingList, favoriteList;
            shoppingList = loadList("shopping_list", "shopping");
            favoriteList = loadList("favorite_list", "favorite");
            Angebot angebot = favoriteList.get(position);
            for (Angebot x : shoppingList) {
                if (x.getTitle().equals(angebot.getTitle())) {
                    return true;
                }
            }
        }
        else {
            ArrayList<Angebot> shoppingList, favoriteList;
            shoppingList = loadList("shopping_list", "shopping");
            favoriteList = loadList("favorite_list_firebase", "favorite");
            Angebot angebot = favoriteList.get(position);
            for (Angebot x : shoppingList) {
                if (x.getTitle().equals(angebot.getTitle())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method listens to changes in the realtime database.
     * If the favorite list of an user changes this method will be invoked.
     * It changes the UI to the current favorites.
     * The user will see in realtime any changes in their favorites list.
     */
    public void updateData() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("users").
            child((Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser())).
            getUid()).child("favorites");
        reference.keepSynced(true);
        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                ArrayList<Angebot> list;
                FavoriteFragment favoriteFragment = (FavoriteFragment) getSupportFragmentManager().
                        findFragmentByTag("FAVORITE_FRAGMENT");
                list = loadList("favorite_list_firebase","favorite");
                Angebot angebot = snapshot.getValue(Angebot.class);
                list.add(angebot);
                saveList(list,"favorite_list_firebase","favorite");
                if (favoriteFragment != null && favoriteFragment.isVisible()) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container1, new FavoriteFragment(),
                                    "FAVORITE_FRAGMENT")
                                    .commit();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //Do nothing
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference reference = database.getReference("users");
                reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                         .child("favorites").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        ArrayList<Angebot> list = new ArrayList<>();
                        FavoriteFragment favoriteFragment = (FavoriteFragment) getSupportFragmentManager()
                                .findFragmentByTag("FAVORITE_FRAGMENT");
                        for (DataSnapshot data: snapshot.getChildren()) {
                            Angebot angebot = data.getValue(Angebot.class);
                            list.add(angebot);
                        }
                        saveList(list,"favorite_list_firebase","favorite");
                        if (favoriteFragment != null && favoriteFragment.isVisible()) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container1, new FavoriteFragment(),
                                            "FAVORITE_FRAGMENT")
                                            .commit();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        //Do nothing
                    }
                });
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //Do nothing
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //Do nothing
            }
        };
        reference.addChildEventListener(childEventListener);
    }

    /**
     * Opens the OptionsActivity.
     */
    @Override
    public void goToOptions() {
        if (loggedIn) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference reference = database.getReference("users").
                    child((Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser())).
                    getUid()).child("favorites");
            reference.removeEventListener(childEventListener);
        }
        Intent intent = new Intent(getApplicationContext(), OptionsActivity.class);
        startActivity(intent);
    }
}