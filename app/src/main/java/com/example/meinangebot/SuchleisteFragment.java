package com.example.meinangebot;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * This fragment manages the requested search.
 * @author Laurin Sasse
 */
public class SuchleisteFragment extends Fragment {

    private EditText editText;
    private FragmentSuchleisteListener listener;
    private FilterListener filterListener;

    /**
     * The interface FilterListener handles the communication between the
     * SuchleisteFragment and the HomeActivity.
     */
    public interface FilterListener {
        void openFilter();
    }

    /**
     * The interface FragmentSuchleisteListener handles the communication between the
     * SuchleisteFragment and the HomeActivity.
     */
    public interface FragmentSuchleisteListener {
        void sendRequest(String string);
    }

    /**
     * This method creates the search bar.
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return returns the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_suchleiste, container, false);
        editText = view.findViewById(R.id.editText);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String requestedText = editText.getText().toString();
                    listener.sendRequest(requestedText);
                }
                return false;
            }
        });
        ImageButton search_button = view.findViewById(R.id.imageButton1);
        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String requestedText = editText.getText().toString();
                listener.sendRequest(requestedText);
            }
        });
        ImageButton filter_button = view.findViewById(R.id.imageButton);
        filter_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterListener.openFilter();
            }
        });
        return view;
    }

    /**
     * This method attaches the listener and the filterListener.
     * @param context context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentSuchleisteListener) {
            listener = (FragmentSuchleisteListener) context;
        }
        if (context instanceof  FilterListener) {
            filterListener = (FilterListener) context;
            return;
        }
        throw new RuntimeException(context.toString()
                + " must implement FragmentSuchleisteListener or FilterListener");

    }

    /**
     * This method detaches the listener and the filterListener.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
        filterListener = null;
    }

}