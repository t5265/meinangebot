package com.example.meinangebot;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * This activity handles the user sign in.
 * @author Laurin Sasse
 */
public class SignInActivity extends AppCompatActivity {

    private EditText editText1, editText2;
    private FirebaseAuth mAuth;
    @SuppressLint("StaticFieldLeak")
    public static Activity fa;

    /**
     * This method displays a UI. So the user can log in.
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        editText1 = findViewById(R.id.editText1);
        editText2 = findViewById(R.id.editText2);
        TextView textView2 = findViewById(R.id.textView2);
        TextView textView3 = findViewById(R.id.textView3);
        TextView textView4 = findViewById(R.id.textView4);
        fa = this;
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrieren();
            }
        });
        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn();
            }
        });
        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword();
            }
        });
        mAuth = FirebaseAuth.getInstance();
    }

    /**
     * This method will open a new Activity in order to register a new user.
     */
    public void registrieren() {
        Intent intent = new Intent (getApplicationContext(), RegistrationActivity.class);
        startActivity(intent);
    }

    /**
     * This method will be used in order to log the user in.
     * If the user fills in the proper registration data, than the user will be logged in.
     */
    public void logIn() {
        String email = editText1.getText().toString();
        String password = editText2.getText().toString();
        if (email.isEmpty()) {
            editText1.setError("Sie müssen einen Wert angeben.");
            editText1.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editText1.setError("Geben Sie eine korrekte Email an.");
            editText1.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            editText2.setError("Sie müssen einen Wert angeben.");
            editText2.requestFocus();
            return;
        }
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    assert user != null;
                    if (user.isEmailVerified()) {
                        FavoriteActivity.fa.finish();
                        Intent intent = new Intent(getApplicationContext(), FavoriteActivity.class);
                        intent.putExtra("Activity_Int", 3);
                        startActivity(intent);
                    }
                    else {
                        user.sendEmailVerification();
                        Toast.makeText(getApplicationContext(), "Eine Email zur Verifikation wurde verschickt.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),"Benutzer konnte nicht eingeloggt werden.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * This method is been used, when a user forgot his password.
     * A new Activity will be started.
     */
    public void forgotPassword() {
        Intent intent = new Intent(getApplicationContext(), ForgotPassword.class);
        startActivity(intent);
    }

}
