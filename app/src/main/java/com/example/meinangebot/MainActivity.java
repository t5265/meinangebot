package com.example.meinangebot;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.auth.FirebaseAuth;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;

/**
 * In the MainActivity all the offers from "Trinkgut" and "Rewe" will be loaded.
 * The offers are saved in an ArrayList and will be send to the HomeActivity with an intent.
 * If the offers are downloaded the HomeActivity will start and the MainActivity will close.
 * <p>
 * Sources:
 *      Source: https://jsoup.org/
 *      Library: jsoup-1.14.3.jar
 *      Version: 1.14.3
 *      License:
 *                        jsoup License
 *      The jsoup code-base (including source and compiled packages) are distributed under the open source MIT license as described below.
 *      The MIT License
 *      Copyright © 2009 - 2021 Jonathan Hedley (https://jsoup.org/)
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 *      associated documentation files (the "Software"),
 *      to deal in the Software without restriction, including without limitation the rights to use, copy,
 *      modify, merge, publish, distribute, sublicense,
 *      and/or sell copies of the Software,
 *      and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 *      ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *      <p>
 *      Source: https://square.github.io/picasso/
 *      Library: com.squareup.picasso:picasso:2.71828
 *      Version: 2.71828
 *      License:
 *                 Copyright 2013 Square, Inc.
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *
 *              http://www.apache.org/licenses/LICENSE-2.0
 *
 *       Unless required by applicable law or agreed to in writing, software
 *       distributed under the License is distributed on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *       See the License for the specific language governing permissions and
 *       limitations under the License.
 *       <p>
 *       Sources to load the images from the supermarkets:
 *
 *       "https://www.trinkgut.de/aktuelle-angebote/bier/?p=1",
 *       "https://www.trinkgut.de/aktuelle-angebote/bier/?p=2",
 *       "https://www.trinkgut.de/aktuelle-angebote/spirituosen/?p=1",
 *       "https://www.trinkgut.de/aktuelle-angebote/spirituosen/?p=2",
 *       "https://www.trinkgut.de/aktuelle-angebote/spirituosen/?p=3",
 *       "https://www.trinkgut.de/aktuelle-angebote/alkoholfreie-getraenke/",
 *       "https://www.trinkgut.de/aktuelle-angebote/wein/",
 *       "https://www.trinkgut.de/aktuelle-angebote/sekt-co./",
 *       "https://www.trinkgut.de/aktuelle-angebote/lebensmittel-mehr/",
 *       "https://www.rewe.de/angebote/lemgo/562036/rewe-markt-entruper-weg-63/?categories=bier",
 *       "https://www.rewe.de/angebote/lemgo/562036/rewe-markt-entruper-weg-63/?categories=topangebote",
 *       "https://www.rewe.de/angebote/lemgo/562036/rewe-markt-entruper-weg-63/?categories=tiefkuehl",
 *       "https://www.rewe.de/angebote/lemgo/562036/rewe-markt-entruper-weg-63/?categories=obst-und-gemuese"
 *       <p>
 *       Source of the App-Logo:
 *       "https://www.primedruck.de/images/categories/1.png" (date: 20.12.2021, 19:30Uhr)
 *       <p>
 * @author Laurin Sasse
 */
public class MainActivity extends AppCompatActivity {

    private ArrayList<Angebot> list;
    private String[] supermarket;
    private String[] url;
    private String[] category;
    private String[] address;
    private int counter;
    private int e;

    /**
     * A new AsyncTask starts.
     * In the AsyncTask the offers will be downloaded and they will be saved in an ArrayList.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseAuth.getInstance().signOut();
        ProgressBarFragment progressBarFragment = new ProgressBarFragment();
        url = new String[]{
                "https://www.trinkgut.de/aktuelle-angebote/bier/?p=1",
                "https://www.trinkgut.de/aktuelle-angebote/bier/?p=2",
                "https://www.trinkgut.de/aktuelle-angebote/spirituosen/?p=1",
                "https://www.trinkgut.de/aktuelle-angebote/spirituosen/?p=2",
                "https://www.trinkgut.de/aktuelle-angebote/spirituosen/?p=3",
                "https://www.trinkgut.de/aktuelle-angebote/alkoholfreie-getraenke/",
                "https://www.trinkgut.de/aktuelle-angebote/wein/",
                "https://www.trinkgut.de/aktuelle-angebote/sekt-co./",
                "https://www.trinkgut.de/aktuelle-angebote/lebensmittel-mehr/",
                "https://www.rewe.de/angebote/lemgo/562036/rewe-markt-entruper-weg-63/?categories=bier",
                "https://www.rewe.de/angebote/lemgo/562036/rewe-markt-entruper-weg-63/?categories=topangebote",
                "https://www.rewe.de/angebote/lemgo/562036/rewe-markt-entruper-weg-63/?categories=tiefkuehl",
                "https://www.rewe.de/angebote/lemgo/562036/rewe-markt-entruper-weg-63/?categories=obst-und-gemuese"};
        supermarket = new String[]{"Trinkgut","Rewe","Edeka"};
        address = new String[]{"Grevenmarschstraße 18 - 22, 32657 Lemgo",
                "Entruper Weg 63, 32657 Lemgo"};
        category = new String[]{"Bier","Spirituosen","Alkoholfreie Getränke","Wein","Sekt und Co.",
                "Lebensmittel und mehr","Topangebote","Tiefkühl","Obst und Gemüse"};
        list = new ArrayList<>();
        counter = 0;
        e = 0;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container1, progressBarFragment)
                .commit();
        Async async = new Async();
        async.execute(url[0], supermarket[0]);
    }

    /**
     * In this method a string will be divided into two strings.
     * The argument identifier contains the string to divide the original string.
     * @param string the string, which will be divided
     * @param identifier the string, which divides the original string
     * @return returns the first string of the char array
     */
    public String editImageUrl(String string, String identifier) {
        String[] arr;
        arr = string.split(identifier);
        return arr[0];
    }

    /**
     * The next AsyncTask starts.
     * This will happen, if the offers from the previous AsyncTask are loaded
     * and stored in the ArrayList.
     * The AsyncTask gets the arguments supermarket and url.
     */
    public void startNextAsyncTask() {
        counter++;
        Async async = new Async();
        async.execute(url[counter], supermarket[e]);
    }

    /**
     * AsyncTask
     */
    @SuppressLint("StaticFieldLeak")
    private class Async extends AsyncTask<String, Integer, ArrayList<Angebot>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * In this method the offers of a specific category will be extracted
         * with the jsoup library and the url.
         * The method doInBackground gets the argument strings, which is a string array
         * and contains in the first element the url.
         * @param strings string array, which contains the first element the url
         * @return returns the ArrayList of the current offers
         */
        @Override
        protected ArrayList<Angebot> doInBackground(String... strings) {
            switch(e) {
                case 0: //Trinkgut
                    try {
                        Document document = Jsoup.connect(strings[0]).get();
                        //The html file will be downloaded and stored in an variable document.
                        String ablaufdatum = document.select("div.content--promotion-period.container").text();
                        //The jsoup library provides a method called void select(String selector),
                        // which allows to filter for certain attributes.
                        //In this case the container of the class content--promotion-period.container
                        //will be extracted and with the method String text() the expiration date will be determined.
                        ablaufdatum = getAblaufdatum(ablaufdatum, "bis ");
                        Elements media = document.select("div.product--info");
                        //All the offers of a certain category will be chosen.
                        int size = media.size();
                        for (int i = 0; i < size; i++) {
                            String img_url = media.select("span.image--media")
                                    .select("img")
                                    .eq(i)
                                    .attr("srcset");
                            //Reads the url of the offer.
                            String title = media.select("span.image--media")
                                    .select("img")
                                    .eq(i)
                                    .attr("title");
                            //Reads the name of the offer.
                            //The variable counter determines the category of the offer.
                            if (counter < 2) {
                                list.add(new Angebot(editImageUrl(img_url, ","), title,
                                    strings[1], category[0], ablaufdatum, null,null, address[0])); //Bier
                            }
                            if (counter >= 2 && counter < 5) {
                                list.add(new Angebot(editImageUrl(img_url, ","), title,
                                    strings[1], category[1], ablaufdatum, null,null, address[0])); //Spirituosen
                            }
                            if (counter == 5) {
                                list.add(new Angebot(editImageUrl(img_url, ","), title,
                                    strings[1], category[2], ablaufdatum, null,null, address[0])); //Alkoholfreie Getränke
                            }
                            if (counter == 6) {
                                list.add(new Angebot(editImageUrl(img_url, ","), title,
                                    strings[1], category[3], ablaufdatum, null, null, address[0])); //Wein
                            }
                            if (counter == 7) {
                                list.add(new Angebot(editImageUrl(img_url, ","), title,
                                    strings[1], category[4], ablaufdatum, null, null, address[0])); //Sekt und Co.
                            }
                            if (counter == 8) {
                                list.add(new Angebot(editImageUrl(img_url, ","), title,
                                    strings[1], category[5], ablaufdatum, null, null, address[0])); //Lebensmittel und mehr
                            }
                        }
                        if (counter == 8) {
                            e++;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 1: //Rewe
                    try {
                        Document document = Jsoup.connect(strings[0]).userAgent("Mozilla").get();
                        String ablaufdatum = document.select("h2.sos-headings__duration").text();
                        ablaufdatum = getAblaufdatum(ablaufdatum, "bis ");
                        Elements media = document.select("div.cor-offer-image");
                        int size = media.size();
                        for (int i = 0; i < size; i++) {
                            media = document.select("div.cor-offer-image");
                            String img_url = media.select("img.cor-lazy")
                                    .eq(i)
                                    .attr("data-src");
                            media = document.select("div.cor-offer-title");
                            String title = media.select("h3")
                                    .eq(i)
                                    .attr("title");
                            title = editTitle(title);
                            media = document.select("div.cor-offer-price");
                            String preis = media.select("div.cor-offer-price-amount").eq(i).text();
                            String rabatt = media.select("div.cor-offer-price-label").eq(i).text();
                            if (counter == 9) {
                                list.add(new Angebot(img_url, title, strings[1], category[0],
                                    ablaufdatum, preis, rabatt, address[1])); //Bier
                            }
                            if (counter == 10) {
                                list.add(new Angebot(img_url, title, strings[1], category[6],
                                    ablaufdatum, preis, rabatt, address[1])); //Topangebote
                            }
                            if (counter == 11) {
                                list.add(new Angebot(img_url, title, strings[1], category[7],
                                    ablaufdatum, preis, rabatt, address[1])); //Tiefkühl
                            }
                            if (counter == 12) {
                                list.add(new Angebot(img_url, title, strings[1], category[8],
                                    ablaufdatum, preis, rabatt, address[1])); //Obst und Gemüse
                            }
                        }
                        if (counter == 12) {
                            e++;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    break;
            }
            return list;
        }

        /**
         * This method edits the title.
         * @param string title
         * @return returns the title
         */
        public String editTitle(String string) {
            String[] arr;
            if (string.contains("\n")) {
                arr = string.split("\n");
                return arr[0] + " " + arr[1];
            }
            else {
                return string;
            }
        }

        /**
         * This method edits the expiration date.
         * @param string expiration date
         * @param splitter identifier to divide the expiration date
         * @return returns the expiration date.
         */
        public String getAblaufdatum(String string, String splitter) {
            String[] arr;
            arr = string.split(splitter);
            return arr[1];
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        /**
         * If all the offers are loaded the ArrayList
         * will be send to the HomeActivity with an intent.
         * If not an new AsyncTask starts. New offers will be loaded.
         * @param angebots ArrayList, that contains all the offers.
         */
        @Override
        protected void onPostExecute(ArrayList<Angebot> angebots) {
            super.onPostExecute(angebots);
            if (counter < url.length - 1) {
                startNextAsyncTask();
            }
            else {
                if (list.size() == 0) {
                    Toast.makeText(getApplicationContext(), "Es konnten keine Angebote geladen werden.",
                            Toast.LENGTH_LONG).show();
                }
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.putParcelableArrayListExtra("AngebotsList", angebots);
                intent.putExtra("Activity_String", "MainActivity");
                intent.putExtra("Activity_Int", 3);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        }
    }
}