package com.example.meinangebot;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * The HomeActivity contains all the offers.
 * In the HomeActivity you can filter and search for certain offers.
 * @author Laurin Sasse
 */
public class HomeActivity extends AppCompatActivity
        implements SuchleisteFragment.FragmentSuchleisteListener, SuchleisteFragment.FilterListener,
        HomeFragment.FragmentChange, MoreInformationFragment.FavoritsListener, MoreInformationFragment.GoogleMapsListener,
        MoreInformationFragment.ShoppingListListener, FilterFragment.FilterFragmentListener {

    private ArrayList<Angebot> list;
    private boolean goBack;
    @SuppressLint("StaticFieldLeak")
    public static Activity fa;

    /**
     * This method displays all the offers.
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String name = extras.getString("Activity_String");
        if (name != null) {
            if (name.equals("MainActivity")) {
                list = intent.getParcelableArrayListExtra("AngebotsList");
                saveList(list, "angebots_list", "angebot");
            }
        }
        else {
            list = intent.getParcelableArrayListExtra("MoreInformation");
        }
        endActivity(extras.getInt("Activity_Int"));
        fa = this;
        if (list == null) {
            list = loadList("angebots_list", "angebot");
            goBack = false;
        }
        if (name == null) {
            goBack = true;
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container1, new SuchleisteFragment())
                .commit();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container2, new HomeFragment(), "StartHomeFragment")
                .commit();
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnItemSelectedListener(navListener);
    }

    //For navigation purpose
    private final BottomNavigationView.OnItemSelectedListener navListener =
            new BottomNavigationView.OnItemSelectedListener() {
                @SuppressLint("NonConstantResourceId")
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Intent intent = null;
                    boolean homeActivity = false;
                    switch (item.getItemId()) {
                        case R.id.home:
                            intent = new Intent(getApplicationContext(), HomeActivity.class);
                            intent.putExtra("Activity_Int", 0);
                            homeActivity = true;
                            break;
                        case R.id.favorites:
                            intent = new Intent(getApplicationContext(), EmptyActivity.class);
                            intent.putExtra("Activity_Int", 6);
                            startActivity(intent);
                            return true;
                        case R.id.shopping_list:
                            intent = new Intent(getApplicationContext(), ShoppingListActivity.class);
                            intent.putExtra("Activity_Int", 0);
                            break;
                    }
                    if (!homeActivity) {
                        startActivity(intent);
                    }
                    else {
                        intent = new Intent(getApplicationContext(), EmptyActivity.class);
                        intent.putExtra("Activity_Int", 0);
                        startActivity(intent);
                    }
                    overridePendingTransition(0,0);
                    return true;
                }
            };

    /**
     * This methods gets the current ArrayList of all offers.
     * @return returns an ArrayList with all offers
     */
    public ArrayList<Angebot> getAngebotsList() {
        return list;
    }

    //For navigation purpose
    public void endActivity(int i) {
        switch (i) {
            case 0:
                HomeActivity.fa.finish();
                break;
            case 1:
                FavoriteActivity.fa.finish();
                break;
            case 2:
                ShoppingListActivity.fa.finish();
                break;
            case 3:
                break;
            case 4:
                EmptyActivity.fa.finish();
                break;
        }
    }

    /**
     * This method filters for certain offers. If the string matches with the
     * title of an offer, it will only show the matching offer.
     * If not a single offer matches with the string equally,
     * than it will show all the offers with a roughly same pattern.
     * @param string string, that the user selected
     */
    @Override
    public void sendRequest(String string) {
        list = loadList("angebots_list","angebot");
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        char []arr;
        int length, score;
        boolean found;
        ArrayList<ScoredAngebot> list1;
        ArrayList<Angebot> list2;
        arr = string.toCharArray();
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        score = 0;
        found = false;
        for (Angebot angebot: list) {
            char []a = angebot.getTitle().toCharArray();
            length = Math.min(a.length, arr.length);
            for(int e = 0; e < length; e++) {
                if(arr[e] == a[e]) {
                    score++;
                }
            }
            list1.add(new ScoredAngebot(angebot, score));
            score = 0;
        }
        length = arr.length;
        for (int i = 0; i < length + 1; i++) {
            for (ScoredAngebot scoredAngebot : list1) {
                if (scoredAngebot.getScore() == length)
                {
                    list2.add(scoredAngebot.getAngebot());
                    found = true;
                    break;
                }
                else if (scoredAngebot.getScore() == length - i) {
                    list2.add(scoredAngebot.getAngebot());
                }
            }
        if (found) {
            break;
        }
        }
        list = list2;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container2, new HomeFragment())
                .commit();
    }

    /**
     * In the first place this method checks, if the user is logged in.
     * If the user is logged in, it checks all the favorite offers in the realtime database
     * for a matching favorite offer.
     * If the user is not logged in, the same procedure will be executed for the local favorites.
     * @param position holds the information about the position
     * @return returns true if the offer is already a favorite and false if not
     */
    public boolean isFavorite(int position) {
        Angebot angebot = list.get(position);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            ArrayList<Angebot> favorites;
            favorites = loadList("favorite_list", "favorite");
            for (Angebot x : favorites) {
                if (x.getTitle().equals(angebot.getTitle())) {
                    return true;
                }
            }
        }
        else {
            ArrayList<Angebot> favorites;
            favorites = loadList("favorite_list_firebase", "favorite");
            for (Angebot x : favorites) {
                if (x.getTitle().equals(angebot.getTitle())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method checks if an offer is in the shopping list.
     * @param position holds the information about the position
     * @return returns true if it is and false if not
     */
    public boolean isInShoppingList(int position) {
        ArrayList<Angebot> shoppingList;
        shoppingList = loadList("shopping_list","shopping");
        Angebot angebot = list.get(position);
        for(Angebot x : shoppingList) {
            if (x.getTitle().equals(angebot.getTitle())) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method changes from the HomeFragment to the MoreInformationFragment.
     */
    @Override
    public void changeFragments(int position) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container2, new EmptyFragment())
                .commit();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container1, new EmptyFragment())
                .commit();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container3, new MoreInformationFragment
                        (list.get(position), position, isFavorite(position), isInShoppingList(position)), "MORE_INFORMATION")
                        .commit();
    }

    /**
     * In this method a favorite will be added to the favorite list, it it is not already a favorite.
     * @param position holds the information about the position
     */
    @Override
    public void addToFavorites(int position) {
        if (!isFavorite(position)) {
            ArrayList<Angebot> favorites;
            favorites = loadList("favorite_list", "favorite");
            Angebot angebot = list.get(position);
            deleteList("favorite_list");
            favorites.add(angebot);
            saveList(favorites, "favorite_list", "favorite");
        }
    }

    /**
     * In this method a favorite will be deleted at the position.
     * @param position holds the information about the position
     */
    @Override
    public void deleteFromFavorites(int position) {
        ArrayList<Angebot> favorites;
        favorites = loadList("favorite_list","favorite");
        Angebot angebot = list.get(position);
        deleteList("favorite_list");
        for (int i=0; i<favorites.size(); i++) {
            if (favorites.get(i).getTitle().equals(angebot.getTitle())) {
                favorites.remove(i);
            }
        }
        saveList(favorites,"favorite_list", "favorite");
    }

    /**
     * In this method the offer at the position will be deleted.
     * @param position holds the information about the position
     */
    public void deleteFromShoppingList(int position) {
        ArrayList<Angebot> shoppingList;
        shoppingList = loadList("shopping_list", "shopping");
        Angebot angebot = list.get(position);
        deleteList("shopping_list");
        for (int i=0; i<shoppingList.size(); i++) {
            if (shoppingList.get(i).getTitle().equals(angebot.getTitle())) {
                shoppingList.remove(i);
            }
        }
        saveList(shoppingList, "shopping_list", "shopping");
    }

    /**
     * In this method a new Activity will be started. This method opens
     * the GoogleMaps app, if it installed on the device.
     * @param intent intent to start GoogleMaps
     */
    @Override
    public void changeToGoogleMaps(Intent intent) {
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
        else {
            Context context = getApplicationContext();
            CharSequence text = "Google Maps wurde nicht gefunden.";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    /**
     * This method loads an ArrayList, that is stored in the local storage of the device.
     * @param list list, that you want to load
     * @param key key in order to access the list
     * @return the list, that is loaded
     */
    public ArrayList<Angebot> loadList(String list, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(list, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<Angebot>>() {}.getType();
        ArrayList<Angebot> x = gson.fromJson(json, type);
        if (x == null) {
            x = new ArrayList<>();
        }
        return x;
    }

    /**
     * This method will delete an ArrayList.
     * @param list list, that will be deleted
     */
    public void deleteList(String list) {
        SharedPreferences sharedPreferences = getSharedPreferences(list, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    /**
     * This method overwrites an ArrayList to a list with a key.
     * The list is stored in the local storage of the device.
     * @param x ArrayList
     * @param list list, that you want to overwrite
     * @param key key in order to access the list
     */
    public void saveList (ArrayList<Angebot> x, String list, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(list, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(x);
        editor.putString(key, json);
        editor.apply();
    }

    //For navigation purpose
    @Override
    public void onBackPressed(){
        Intent intent;
        MoreInformationFragment moreInformationFragment =
                (MoreInformationFragment) getSupportFragmentManager().findFragmentByTag("MORE_INFORMATION");
        HomeFragment homeFragment =
                (HomeFragment) getSupportFragmentManager().findFragmentByTag("FILTER_HOME_FRAGMENT");
        if (moreInformationFragment != null && moreInformationFragment.isVisible()) {
            intent = new Intent(getApplicationContext(), EmptyActivity.class);
            intent.putExtra("Activity_Int", 0);
            intent.putParcelableArrayListExtra("MoreInformation", list);
            startActivity(intent);
        }
        else if (goBack) {
            intent = new Intent(getApplicationContext(), EmptyActivity.class);
            intent.putExtra("Activity_Int", 0);
            startActivity(intent);
        }
        else if (homeFragment != null && homeFragment.isVisible()) {
            intent = new Intent(getApplicationContext(), EmptyActivity.class);
            intent.putExtra("Activity_Int", 0);
            startActivity(intent);
        }
        else {
            super.onBackPressed();
        }
    }

    /**
     * This method opens the FilterFragment.
     */
    @Override
    public void openFilter() {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in,R.anim.nav_default_exit_anim,
                        R.anim.nav_default_pop_enter_anim, R.anim.slide_out)
                    .replace(R.id.fragment_container4, new FilterFragment())
                    .addToBackStack("HomeActivity")
                    .commit();
    }

    /**
     * This method adds an offer to the shopping list.
     * @param position holds the information about the position
     */
    @Override
    public void addToShoppingList(int position) {
        if (!isInShoppingList(position)) {
            ArrayList<Angebot> shoppingList;
            shoppingList = loadList("shopping_list", "shopping");
            Angebot angebot = list.get(position);
            deleteList("shopping_list");
            shoppingList.add(angebot);
            saveList(shoppingList, "shopping_list", "shopping");
        }
    }

    /**
     * This method will apply the filters an look for the selected offers.
     * @param category category, that has been selected
     * @param supermarket supermarket, that has been selected
     * @param ablaufdatum ablaufdatum has no function, can be added in the future
     */

    @Override
    public void applyFilters(String category, String supermarket, boolean ablaufdatum) {
        list = loadList("angebots_list","angebot");
        ArrayList<Angebot> list1 = new ArrayList<>();
        int size;
        size = list.size();
        for (int i=0; i<size; i++) {
            if (list.get(i).getKategorie().equals(category) || category.equals("Alle Kategorien")) {
                if (list.get(i).getSupermarket().equals(supermarket) || supermarket.equals("Alle Supermärkte")) {
                    list1.add(list.get(i));
                }
            }
        }
        list = list1;
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container2, new HomeFragment(),
            "FILTER_HOME_FRAGMENT")
                .commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container4, new EmptyFragment())
                .commit();
    }
}