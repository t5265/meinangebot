package com.example.meinangebot;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.widget.ImageViewCompat;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * This fragment handles the more detailed view of an offer.
 * It also handles, when you want to add it to favorites or the shopping list.
 * @author Laurin Sasse
 */
public class MoreInformationFragment extends Fragment {

    private ImageView imageView;
    private ImageButton imageButton1;
    private ImageButton imageButton2;
    private final Angebot angebot;
    private FavoritsListener favoritsListener;
    private ShoppingListListener shoppingListListener;
    private GoogleMapsListener googleMapsListener;
    private final int position;
    private boolean favorite;
    private boolean isShoppingList;

    /**
     * The interface FavoritsListener handles the communication between the
     * MoreInformationFragment and the HomeActivity.
     */
    public interface FavoritsListener {
        void addToFavorites(int position);
        void deleteFromFavorites(int position);
    }

    /**
     * The interface GoogleMapsListener handles the communication between the
     * MoreInformationFragment and the HomeActivity.
     */
    public interface GoogleMapsListener {
        void changeToGoogleMaps(Intent intent);
    }

    /**
     * The interface ShoppingListListener handles the communication between the
     * MoreInformationFragment and the HomeActivity.
     */
    public interface ShoppingListListener {
        void addToShoppingList(int position);
        void deleteFromShoppingList(int position);
    }

    /**
     * The constructor defines the offer, the position.
     * If it is a favorite or if it is in the shopping list isShoppingList.
     * @param angebot offer
     * @param position position
     * @param favorite favorite
     * @param isShoppingList isShoppingList
     */
    public MoreInformationFragment (Angebot angebot, int position,
                                    boolean favorite, boolean isShoppingList) {
        this.angebot = angebot;
        this.position = position;
        this.favorite = favorite;
        this.isShoppingList = isShoppingList;
    }

    /**
     * This method displays the more detailed offer with its attributes.
     * It also decides between two cases. The cases are logged in or not.
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return returns the view
     */
    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more_information, container, false);
        imageView = view.findViewById(R.id.imageView1);
        imageButton1 = view.findViewById(R.id.imageButton1);
        imageButton2 = view.findViewById(R.id.imageButton2);
        ImageButton imageButton3 = view.findViewById(R.id.imageButton3);
        TextView textView1 = view.findViewById(R.id.textView1);
        TextView textView2 = view.findViewById(R.id.textView2);
        TextView textView3 = view.findViewById(R.id.textView3);
        TextView textView4 = view.findViewById(R.id.textView4);
        TextView textView5 = view.findViewById(R.id.textView5);
        TextView textView6 = view.findViewById(R.id.textView6);
        textView1.setText("Artikel: "+angebot.getTitle());
        textView2.setText("Markt: "+angebot.getSupermarket());
        textView3.setText("Kategorie: "+angebot.getKategorie());
        textView4.setText("Ablaufdatum: "+ angebot.getAblaufdatum());
        if (angebot.getPreis()!=null) {
            textView5.setText("Preis: " + angebot.getPreis()+"€");
        }
        if (angebot.getRabatt()!=null) {
            textView6.setText("Rabatt: " + angebot.getRabatt());
        }
        Picasso.get().load(angebot.getImage_url()).into(getImageView());
        imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!favorite) {
                    ImageViewCompat.setImageTintList(imageButton1, ColorStateList.valueOf(0xffff0000));
                    favorite = true;
                    if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                        favoritsListener.addToFavorites(position);
                    }
                    else {
                        ArrayList<Angebot> list = new ArrayList<>();
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference reference = database.getReference("users");
                        reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .child("favorites").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                for (DataSnapshot data : snapshot.getChildren()) {
                                    Angebot angebot_firebase = data.getValue(Angebot.class);
                                    list.add(angebot_firebase);
                                }
                                for (Angebot favoriten: list) {
                                    if (favoriten.getTitle().equals(angebot.getTitle())) {
                                        return;
                                    }
                                }
                                list.add(angebot);
                                reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .child("favorites")
                                        .setValue(list).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()) {
                                            Toast.makeText(getContext(), "Angebot wurde hinzugefügt.",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                        else {
                                            Toast.makeText(getContext(),"Angebot konnte nicht hinzugefügt werden.",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                                Toast.makeText(getContext(),"Angebot konnte nicht hinzugefügt werden.",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
                else {
                    ImageViewCompat.setImageTintList(imageButton1, ColorStateList.valueOf(0xff000000));
                    favorite = false;
                    if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                        favoritsListener.deleteFromFavorites(position);
                    }
                    else {
                        String[] key = new String[1];
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference reference = database.getReference("users");
                        reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .child("favorites").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                for (DataSnapshot data : snapshot.getChildren()) {
                                    Angebot angebot_firebase = data.getValue(Angebot.class);
                                    assert angebot_firebase != null;
                                    if (angebot_firebase.getTitle().equals(angebot.getTitle())) {
                                        key[0] = data.getKey();
                                    }
                                }
                                reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .child("favorites").child(key[0])
                                        .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()) {
                                            Toast.makeText(getContext(), "Favorit wurde entfernt.",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                        else {
                                            Toast.makeText(getContext(),"Favorit konnte nicht entfernt werden.",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                                Toast.makeText(getContext(),"Favorit konnte nicht entfernt werden.",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            }
        });
        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isShoppingList) {
                    ImageViewCompat.setImageTintList(imageButton2, ColorStateList.valueOf(0xffff0000));
                    shoppingListListener.addToShoppingList(position);
                    isShoppingList = true;
                }
                else {
                    ImageViewCompat.setImageTintList(imageButton2, ColorStateList.valueOf(0xff000000));
                    shoppingListListener.deleteFromShoppingList(position);
                    isShoppingList = false;
                }
            }
        });
        imageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q="+angebot.getAdresse());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                googleMapsListener.changeToGoogleMaps(mapIntent);
            }
        });
        if (favorite) {
            ImageViewCompat.setImageTintList(imageButton1, ColorStateList.valueOf(0xffff0000));
        }
        if (isShoppingList) {
            ImageViewCompat.setImageTintList(imageButton2, ColorStateList.valueOf(0xffff0000));
        }
        return view;
    }

    /**
     * @return returns the imageView
     */
    public ImageView getImageView() {
        return imageView;
    }

    /**
     * This method attaches the favoritsListener, the googleMapsListener and the shoppingListListener.
     * @param context context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FavoritsListener) {
            favoritsListener = (FavoritsListener) context;
        }
        if (context instanceof GoogleMapsListener) {
            googleMapsListener = (GoogleMapsListener) context;
        }
        if (context instanceof  ShoppingListListener) {
            shoppingListListener = (ShoppingListListener) context;
            return;
        }
        throw new RuntimeException(context.toString()
                + " must implement FavoritsListener or GoogleMapsListener or ShoppingListListener");
    }

    /**
     * This method detaches the favoritsListener, the googleMapsListener and the shoppingListListener.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        favoritsListener= null;
        googleMapsListener = null;
        shoppingListListener = null;
    }

}