package com.example.meinangebot;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * This activity manages the options, here you can delete your account.
 */
public class OptionsActivity extends AppCompatActivity {

    /**
     * This method displays a textView, that allows the user to delete their account.
     * If the textView is pressed the account will be deleted.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        TextView textView1;
        textView1 = findViewById(R.id.textView1);
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                    Toast.makeText(getApplicationContext(),"Sie müssen eingeloggt sein.",
                            Toast.LENGTH_LONG).show();
                }
                else
                {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference reference = database.getReference("users");
                    reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
                    Toast.makeText(getApplicationContext(),"Ihr Account wurde gelöscht.",
                            Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            }
        });
    }
}