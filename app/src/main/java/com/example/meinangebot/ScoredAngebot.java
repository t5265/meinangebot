package com.example.meinangebot;

/**
 /**
 * This class declares all the attributes of a scored offer.
 * @author Laurin Sasse
 */
public class ScoredAngebot {

    int score;
    Angebot angebot;

    public ScoredAngebot(Angebot angebot, int score) {
        this.angebot = angebot;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public Angebot getAngebot() {
        return angebot;
    }
}
