package com.example.meinangebot;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * The ShoppingListActivity manages all the offers in the shopping list.
 * @author Laurin Sasse
 */
public class ShoppingListActivity extends AppCompatActivity
        implements ShoppingListFragment.ShoppingListListener, ShoppingListFragment.SendShoppingListListener {

    @SuppressLint("StaticFieldLeak")
    public static Activity fa;
    private boolean weiter;
    private boolean stop;

    /**
     * This method displays all the offers in the shopping list.
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        endActivity(extras.getInt("Activity_Int"));
        fa = this;
        ShoppingListFragment shoppingListFragment;
        shoppingListFragment = new ShoppingListFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container1, shoppingListFragment)
                .commit();
        weiter = false;
        stop = false;
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.shopping_list);
        bottomNavigationView.setOnItemSelectedListener(navListener);
        ActivityCompat.requestPermissions(ShoppingListActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
    }

    //For navigation purpose
    private final BottomNavigationView.OnItemSelectedListener navListener =
            new BottomNavigationView.OnItemSelectedListener() {
                @SuppressLint("NonConstantResourceId")
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Intent intent = null;
                    boolean shoppingListActivity = false;
                    switch (item.getItemId()) {
                        case R.id.home:
                            intent = new Intent(getApplicationContext(), HomeActivity.class);
                            intent.putExtra("Activity_Int",2);
                            break;
                        case R.id.favorites:
                            intent = new Intent(getApplicationContext(), EmptyActivity.class);
                            intent.putExtra("Activity_Int",5);
                            startActivity(intent);
                            return true;
                        case R.id.shopping_list:
                            shoppingListActivity = true;
                            break;
                    }
                    if (!shoppingListActivity) {
                        startActivity(intent);
                    }
                    else {
                        intent = new Intent(getApplicationContext(), EmptyActivity.class);
                        intent.putExtra("Activity_Int", 2);
                        startActivity(intent);
                    }
                    overridePendingTransition(0,0);
                    return true;
                }
            };

    //For navigation purpose
    public void endActivity(int i) {
        switch (i) {
            case 0:
                HomeActivity.fa.finish();
                break;
            case 1:
                FavoriteActivity.fa.finish();
                break;
            case 2:
                ShoppingListActivity.fa.finish();
                break;
            case 3:
                break;
            case 4:
                EmptyActivity.fa.finish();
                break;
        }
    }

    /**
     * This method loads an ArrayList, that is stored in the local storage of the device.
     * @param list list, that you want to load
     * @param key key in order to access the list
     * @return the list, that is loaded
     */
    public ArrayList<Angebot> loadList(String list, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(list, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<Angebot>>() {}.getType();
        ArrayList<Angebot> x = gson.fromJson(json, type);
        if (x == null) {
            x = new ArrayList<>();
        }
        return x;
    }

    /**
     * This method overwrites an ArrayList to a list with a key.
     * The list is stored in the local storage of the device.
     * @param x ArrayList
     * @param list list, that you want to overwrite
     * @param key key in order to access the list
     */
    public void saveList (ArrayList<Angebot> x, String list, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(list, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(x);
        editor.putString(key, json);
        editor.apply();
    }

    /**
     * This method will delete a certain offer.
     * @param position holds the information about the position
     */
    @Override
    public void deleteElement(int position) {
        ArrayList<Angebot> shoppingList = loadList("shopping_list", "shopping");
        shoppingList.remove(position);
        saveList(shoppingList, "shopping_list", "shopping");
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container1, new ShoppingListFragment())
                .commit();
    }

    /**
     * This method will create a PDF document.
     */
    @Override
    public void createPDF() {
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(600, 875, 1).create();
        Paint paint = new Paint();
        Paint text = new Paint();
        ArrayList<Angebot> list = loadList("shopping_list", "shopping");
        int length = list.size();
        int page_length = (int) Math.ceil(length/5.0);
        stop = false;
        createPages(list, pdfDocument, paint, text, pageInfo, page_length);
        String path = getExternalFilesDir(null).getAbsolutePath() +"/angebot.pdf";
        File file = new File(path);
        try {
            pdfDocument.writeTo(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        pdfDocument.close();
        sendPDF(file);
    }

    /**
     * This method sends the PDF document.
     * @param file file, that contains the PDF document
     */
    public void sendPDF(File file) {
        stop = true;
        if (file.exists()) {
            Intent send = new Intent(Intent.ACTION_SEND);
            send.setType("application/pdf");
            send.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            startActivity(Intent.createChooser(send, "Send Angebot"));
        }
        else {
            Toast.makeText(this, "File doesn't exist.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This method creates the pages for a pdf document. Only 5 offers per page are allowed.
     * @param list list, that creates the pdf
     * @param pdfDocument pdfDocument
     * @param paint paint
     * @param text text
     * @param pageInfo pageInfo
     * @param page_length 5 offers per page
     */
    public void createPages(ArrayList<Angebot> list, PdfDocument pdfDocument, Paint paint,
                            Paint text, PdfDocument.PageInfo pageInfo, int page_length) {
        int counter = 0;
        int shopping_list_length = list.size();
        int loop_length;
        while(counter != page_length) {
            if (shopping_list_length<6) {
                loop_length = shopping_list_length;
            }
            else {
                loop_length = 5;
                shopping_list_length = shopping_list_length - 5;
            }
            PdfDocument.Page page = pdfDocument.startPage(pageInfo);
            text.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
            text.setTextSize(20);
            page.getCanvas().drawLine(175, 0, 175, 1000, paint);
            for (int i = 0; i < loop_length; i++) {
                String name = "Name :";
                String supermarkt = "Supermarkt :";
                String ablaufdatum = "Ablaufdatum :";
                String preis = "Preis :";
                String p_preis;
                String p_name = list.get(i+counter*5).getTitle();
                String p_supermarkt = list.get(i+counter*5).getSupermarket();
                String p_ablaufdatum = list.get(i+counter*5).getAblaufdatum();
                if (list.get(i+counter*5).getPreis()!=null) {
                    p_preis = list.get(i+counter*5).getPreis()+"€";
                }
                else {
                    preis = "";
                    p_preis = "";
                }
                page.getCanvas().drawText(name, 200, i * 175 + 25, text);
                page.getCanvas().drawText(p_name, 350, i * 175 + 25, text);
                page.getCanvas().drawText(supermarkt, 200, i * 175 + 50, text);
                page.getCanvas().drawText(p_supermarkt, 350, i * 175 + 50, text);
                page.getCanvas().drawText(ablaufdatum, 200, i * 175 + 75, text);
                page.getCanvas().drawText(p_ablaufdatum, 350, i * 175 + 75, text);
                page.getCanvas().drawText(preis, 200, i * 175 + 100, text);
                page.getCanvas().drawText(p_preis, 350, i * 175 + 100, text);
                page.getCanvas().drawLine(0, i * 175 + 175, 600, i * 175 + 175, paint);
                weiter = false;
                while (!weiter) {
                    Canvas canvas = page.getCanvas();
                    int finalI = i * 175 + 12;
                    Picasso.get().load(loadList("shopping_list", "shopping")
                            .get(i+counter*5).getImage_url()).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            if (!stop) {
                                bitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, false);
                                canvas.drawBitmap(bitmap, 0, finalI, paint);
                                weiter = true;
                            }
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
                }
            }
            pdfDocument.finishPage(page);
            counter++;
        }
    }

}