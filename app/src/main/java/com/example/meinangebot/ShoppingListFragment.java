package com.example.meinangebot;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;

/**
 * This fragment displays all the offers, that are stored in the shopping list.
 * @author Laurin Sasse
 */
public class ShoppingListFragment extends Fragment
        implements RecyclerViewAdapterShoppingList.SwipeLeftListener,
        RecyclerViewAdapterShoppingList.SwipeRightListener {

    private RecyclerView recyclerView;
    private ShoppingListListener shoppingListListener;
    private SendShoppingListListener sendShoppingListListener;

    /**
     * This interface handles the communication between the the ShoppingListFragment and the
     * ShoppingListActivity.
     */
    public interface SendShoppingListListener {
        void createPDF();
    }

    /**
     * This interface handles the communication between the the ShoppingListFragment and the
     * ShoppingListActivity.
     */
    public interface ShoppingListListener {
        void deleteElement(int position);
    }

    /**
     * In this method all the current offers, that are stored in the shopping list will be displayed.
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return returns the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_list, container, false);
        FloatingActionButton floatingActionButton = view.findViewById(R.id.floatingActionButton);
        recyclerView = view.findViewById(R.id.recyclerView);
        setRecyclerView(((ShoppingListActivity) view.getContext()).loadList("shopping_list","shopping"));
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendShoppingListListener.createPDF();
            }
        });
        return view;
    }

    /**
     * This method sets the RecyclerView.
     * @param list ArrayList, that transfers all the offers to the RecyclerView
     */
    public void setRecyclerView(ArrayList<Angebot> list) {
        RecyclerViewAdapterShoppingList recyclerViewAdapterShoppingList =
                new RecyclerViewAdapterShoppingList(list, this, this);
        recyclerView.setAdapter(recyclerViewAdapterShoppingList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    /**
     * This will delete the offer at the certain position
     * @param position holds the information about the position
     */
    @Override
    public void swipeLeft(int position) {
        shoppingListListener.deleteElement(position);
    }


    @Override
    public void swipeRight(int position) {
        //Do nothing
    }

    /**
     * This method attaches the shoppingListlistener and the sendShoppingListListener.
     * @param context context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ShoppingListFragment.ShoppingListListener) {
            shoppingListListener = (ShoppingListFragment.ShoppingListListener) context;
        }
        else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentSuchleisteListener");
        }
        if (context instanceof ShoppingListFragment.SendShoppingListListener) {
            sendShoppingListListener = (ShoppingListFragment.SendShoppingListListener) context;
        }
        else {
            throw new RuntimeException(context.toString()
                    + " must implement SendShoppingListListener");
        }
    }

    /**
     * This method detaches the shoppingListListener and the sendShoppingListListener.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        shoppingListListener = null;
        sendShoppingListListener = null;
    }

}