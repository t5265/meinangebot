package com.example.meinangebot;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

/**
 * The HomeFragment displays all the offers in the HomeActivity.
 * @author Laurin Sasse
 */
public class HomeFragment extends Fragment
        implements RecyclerViewAdapter.ImageButton1Listener, RecyclerViewAdapter.ImageButton2Listener{

    private RecyclerView recyclerView;
    private FragmentChange listener;

    /**
     * The interface FragmentChange invokes the method void changeFragments(...).
     * It will open the MoreInformationFragment with the selected offer.
     */
    public interface FragmentChange {
        void changeFragments(int position);
    }

    /**
     * This method displays all the offers in the HomeActivity.
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return returns the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        setRecyclerView(((HomeActivity) view.getContext()).getAngebotsList());
        return view;
    }

    /**
     * This method sets the RecyclerView.
     * @param list ArrayList, that transfers all the offers to the RecyclerView
     */
    public void setRecyclerView(ArrayList<Angebot> list) {
        RecyclerViewAdapter recyclerViewAdapter =
                new RecyclerViewAdapter(list, this, this);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    /**
     * If the left image is clicked, it will invoke the
     * method void changeFragments(...) in the HomeActivity.
     * @param position holds the information about the position
     */
    @Override
    public void imageButton1(int position) {
        listener.changeFragments(2*position);
    }

    /**
     * If the right image is clicked, it will invoke the
     * method void changeFragments(...) in the HomeActivity.
     * @param position holds the information about the position
     */
    @Override
    public void imageButton2(int position) {
        listener.changeFragments(2*position+1);
    }

    /**
     * This method attaches listener.
     * @param context context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentChange) {
            listener = (FragmentChange) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentChange");
        }
    }

    /**
     * This method detaches the listener.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}