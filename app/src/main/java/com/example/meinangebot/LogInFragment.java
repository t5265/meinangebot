package com.example.meinangebot;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * This Fragment handles the log in.
 * If you want to sign in, it will open the Activity SignInActivity.
 * You can also choose the Activity OptionsActivity.
 * It will also signalise whether you are logged in or not.
 * @author Laurin Sasse
 */
public class LogInFragment extends Fragment {

    private boolean loggedIn;
    private LogInFragmentListener logInFragmentListener;

    /**
     * This interface handles the communication between the LogInFragment and the FavoriteActivity.
     * If the method void goToOptions(..) is invoked, it will change the Activities to
     * the OptionsActivity.
     */
    public interface LogInFragmentListener {
        void goToOptions();
    }

    /**
     * This fragment displays the LogInFragment. It also checks, if the user is logged in or not.
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return returns the view
     */
    @SuppressLint("SimpleDateFormat")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_in, container, false);
        Calendar calendar;
        String date;
        SimpleDateFormat simpleDateFormat;
        TextView textView1;
        ImageButton imageButton1, imageButton2, imageButton3;
        imageButton1 = view.findViewById(R.id.imageButton1);
        if (loggedIn) {
            imageButton1.setVisibility(View.INVISIBLE);
        }
        imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!loggedIn) {
                    Intent intent = new Intent(getContext(), SignInActivity.class);
                    startActivity(intent);
                }
            }
        });
        imageButton2 = view.findViewById(R.id.imageButton2);
        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logInFragmentListener.goToOptions();
            }
        });
        imageButton3 = view.findViewById(R.id.imageButton3);
        if (!loggedIn) {
            imageButton3.setVisibility(View.INVISIBLE);
        }
        imageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loggedIn) {
                    FirebaseAuth.getInstance().signOut();
                    Toast.makeText(getContext(), "Sie wurden ausgeloggt.",
                            Toast.LENGTH_SHORT).show();
                    imageButton3.setVisibility(View.INVISIBLE);
                    imageButton1.setVisibility(View.VISIBLE);
                    loggedIn = false;
                    Intent intent = new Intent(getContext(), EmptyActivity.class);
                    intent.putExtra("Activity_Int", 1);
                    startActivity(intent);
                }
            }
        });
        textView1 = view.findViewById(R.id.textView1);
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        date = simpleDateFormat.format(calendar.getTime());
        textView1.setText(date);
        return view;
    }

    /**
     * If a new LogInFragment is created.
     * It will receive an information, whether the user is loggedIn.
     * @param loggedIn
     */
    public LogInFragment (boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    /**
     * This method attaches logInFragmentListener.
     * @param context context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof LogInFragment.LogInFragmentListener) {
            logInFragmentListener = (LogInFragment.LogInFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LogInFragmentListener");
        }
    }

    /**
     * This method detaches the logInFragmentListener.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        logInFragmentListener = null;
    }

}