package com.example.meinangebot;

import android.annotation.SuppressLint;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * This class defines the RecyclerViewAdapter for recycler view in the ShoppingListActivity.
 * Sources:
 *          Source: https://square.github.io/picasso/
 *          Library: com.squareup.picasso:picasso:2.71828
 *          Version: 2.71828
 *          License:
 *                      Copyright 2013 Square, Inc.
 *
 *          Licensed under the Apache License, Version 2.0 (the "License");
 *          you may not use this file except in compliance with the License.
 *          You may obtain a copy of the License at
 *
 *              http://www.apache.org/licenses/LICENSE-2.0
 *
 *          Unless required by applicable law or agreed to in writing, software
 *          distributed under the License is distributed on an "AS IS" BASIS,
 *          WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *          See the License for the specific language governing permissions and
 *          limitations under the License.
 *
 * @author Laurin Sasse
 */
public class RecyclerViewAdapterShoppingList extends RecyclerView.Adapter<RecyclerViewAdapterShoppingList.ViewHolder> {

    private final ArrayList<Angebot> list;
    private final SwipeLeftListener swipeLeftListener;
    private final SwipeRightListener swipeRightListener;

    public interface SwipeLeftListener {
        void swipeLeft(int position);
    }

    public interface SwipeRightListener {
        void swipeRight(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView1;
        TextView textView1, textView2, textView3, textView4, textView5, textView6, textView7, textView8;
        ImageButton imageButton1;

        public ViewHolder(View view, RecyclerViewAdapterShoppingList.SwipeLeftListener swipeLeftListener,
                          RecyclerViewAdapterShoppingList.SwipeRightListener swipeRightListener) {
            super(view);
            imageView1 = view.findViewById(R.id.imageView1);
            imageButton1 = view.findViewById(R.id.imageButton1);
            textView1 = view.findViewById(R.id.textView1);
            textView2 = view.findViewById(R.id.textView2);
            textView3 = view.findViewById(R.id.textView3);
            textView4 = view.findViewById(R.id.textView4);
            textView5 = view.findViewById(R.id.textView5);
            textView6 = view.findViewById(R.id.textView6);
            textView7 = view.findViewById(R.id.textView7);
            textView8 = view.findViewById(R.id.textView8);
            imageButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    swipeLeftListener.swipeLeft(getAdapterPosition());
                }
            });
            final GestureDetector gesture = new GestureDetector(view.getContext(),
                    new GestureDetector.SimpleOnGestureListener() {
                        @Override
                        public boolean onDown(MotionEvent e) {
                            return true;
                        }

                        @Override
                        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                               float velocityY) {

                            final int distanz = 40;
                            final int abweichung = 250;
                            final int geschwindigkeit = 100;
                            try {
                                if (Math.abs(e1.getY() - e2.getY()) > abweichung)
                                    return false;
                                if (e1.getX() - e2.getX() > distanz
                                        && Math.abs(velocityX) > geschwindigkeit) {
                                    swipeLeftListener.swipeLeft(getAdapterPosition());
                                } else if (e2.getX() - e1.getX() > distanz
                                        && Math.abs(velocityX) > geschwindigkeit) {
                                    swipeRightListener.swipeRight(getAdapterPosition());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return super.onFling(e1, e2, velocityX, velocityY);
                        }
                    });
            view.setOnTouchListener(new View.OnTouchListener() {
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gesture.onTouchEvent(event);
                }
            });
        }

        public ImageView getImageView1() {
            return imageView1;
        }

        public ImageButton getImageButton1() {
            return imageButton1;
        }

        public TextView getTextView1() {
            return textView1;
        }

        public TextView getTextView2() {
            return textView2;
        }

        public TextView getTextView3() {
            return textView3;
        }

        public TextView getTextView4() {
            return textView4;
        }

        public TextView getTextView5() {
            return textView5;
        }

        public TextView getTextView6() {
            return textView6;
        }

        public TextView getTextView7() {
            return textView7;
        }

        public TextView getTextView8() {
            return textView8;
        }

    }

    public RecyclerViewAdapterShoppingList(ArrayList<Angebot> list, SwipeLeftListener swipeLeftListener,
                                           SwipeRightListener swipeRightListener) {
        this.list = list;
        this.swipeLeftListener = swipeLeftListener;
        this.swipeRightListener = swipeRightListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.shopping_list_dummy, viewGroup, false);
        return new ViewHolder(view, swipeLeftListener, swipeRightListener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get().load(list.get(position).getImage_url())
                .resize(300, 300).centerCrop().into(holder.getImageView1());
        holder.getTextView1().setText("Artikelname: ");
        holder.getTextView5().setText(list.get(position).getTitle());
        holder.getTextView2().setText("Supermarkt: ");
        holder.getTextView6().setText(list.get(position).getSupermarket());
        holder.getTextView3().setText("Ablaufdatum: ");
        holder.getTextView7().setText(list.get(position).getAblaufdatum());
        if (list.get(position).getSupermarket().equals("Trinkgut")) {
            holder.getTextView4().setText(" ");
            holder.getTextView8().setText(" ");
        }
        else {
            holder.getTextView4().setText("Preis: ");
            holder.getTextView8().setText(list.get(position).getPreis()+"€");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
