package com.example.meinangebot;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import java.util.Objects;

/**
 * This activity handles the user registration.
 * @author Laurin Sasse
 */
public class RegistrationActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText first_name, last_name, email, password;

    /**
     * This method displays the UI for the user registration.
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mAuth = FirebaseAuth.getInstance();
        first_name = findViewById(R.id.editText1);
        last_name = findViewById(R.id.editText2);
        email = findViewById(R.id.editText3);
        password = findViewById(R.id.editText4);
        TextView endRegistration = findViewById(R.id.textView6);
        endRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUser();
            }
        });
    }

    /**
     * This method proves, whether the user entered all the information correctly.
     * If done correctly, the user will be added to the FireBase realtime database.
     */
    public void addUser() {
        String string_first_name, string_last_name, string_email, string_password;
        string_first_name = first_name.getText().toString();
        string_last_name = last_name.getText().toString();
        string_email = email.getText().toString();
        string_password = password.getText().toString();
        if(string_first_name.isEmpty()) {
            first_name.setError("Sie müssen einen Wert angeben.");
            first_name.requestFocus();
            return;
        }
        if(string_last_name.isEmpty()) {
            last_name.setError("Sie müssen einen Wert angeben.");
            last_name.requestFocus();
            return;
        }
        if(string_email.isEmpty()) {
            email.setError("Sie müssen einen Wert angeben.");
            email.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(string_email).matches()) {
            email.setError("Geben Sie eine korrekte E-Mail an.");
            email.requestFocus();
            return;
        }
        if(string_password.isEmpty()) {
            password.setError("Sie müssen einen Wert angeben.");
            password.requestFocus();
            return;
        }
        if(string_password.length() < 6) {
            password.setError("Das Passwort muss mindestens 7 Zeichen haben");
            password.requestFocus();
        }
        mAuth.createUserWithEmailAndPassword(string_email, string_password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            User user = new User(string_first_name, string_last_name, string_email, string_password);
                            FirebaseDatabase.getInstance().getReference("users")
                                    .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()) {
                                        Toast.makeText(getApplicationContext(), "Benutzer wurde registriert.",
                                                Toast.LENGTH_LONG).show();
                                        onBackPressed();
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(),"Benutzer konnte nicht hinzugefügt werden.",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Benutzer konnte nicht hinzugefügt werden.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

}