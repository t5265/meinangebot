package com.example.meinangebot;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * This class declares all the attributes of an offer.
 * @author Laurin Sasse
 */
public class Angebot implements Parcelable {

    private String image_url;
    private String title;
    private String supermarket;
    private String kategorie;
    private String ablaufdatum;
    private String preis;
    private String rabatt;
    private String adresse;

    /**
     * An empty constructor is needed for the FireBase API.
     */
    public Angebot() {
        //Empty constructor
    }

    public Angebot(String image_url, String title, String supermarket,
                   String kategorie, String ablaufdatum, String preis,
                   String rabatt, String adresse) {
        this.image_url = image_url;
        this.title = title;
        this.supermarket = supermarket;
        this.kategorie = kategorie;
        this.ablaufdatum = ablaufdatum;
        this.preis = preis;
        this.rabatt = rabatt;
        this.adresse = adresse;
    }

    protected Angebot(Parcel in) {
        image_url = in.readString();
        title = in.readString();
        supermarket = in.readString();
        kategorie = in.readString();
        ablaufdatum = in.readString();
        preis = in.readString();
        rabatt = in.readString();
        adresse = in.readString();
    }

    public static final Creator<Angebot> CREATOR = new Creator<Angebot>() {
        @Override
        public Angebot createFromParcel(Parcel in) {
            return new Angebot(in);
        }

        @Override
        public Angebot[] newArray(int size) {
            return new Angebot[size];
        }
    };

    public String getImage_url() {
        return image_url;
    }

    public String getKategorie() {
        return kategorie;
    }

    public String getAblaufdatum() {
        return ablaufdatum;
    }
    
    public String getPreis() {
        return preis;
    }
    
    public String getRabatt() {
        return rabatt;
    }

    public String getTitle() {
        return title;
    }

    public String getSupermarket() {
        return supermarket;
    }

    public String getAdresse() {
        return adresse;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image_url);
        dest.writeString(title);
        dest.writeString(supermarket);
        dest.writeString(kategorie);
        dest.writeString(ablaufdatum);
        dest.writeString(preis);
        dest.writeString(rabatt);
        dest.writeString(adresse);
    }
}
